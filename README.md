# omeka.fr

## Processus d’installation et de mise à jour

Les installations initiales de Omeka Classic, des extensions et des thèmes ont été réalisées à partir de leurs dépôts Github (ou Gitlab pour le thème de omeka.fr) respectifs. Les mises à jours s’effectuent donc via ces mêmes dépôts. 

Procédure générique 
- `cd` _chemin_         # chemin de la racine du logiciel à mettre à jour
- `git pull`
- `git tag`             # pour récupérer le libellé de la dernière version officielle  
- `git checkout` _tag_  #si le libellé tag existe (cf étape précédente)

compte : omeka-fr

### Omeka Classic

https://github.com/omeka/Omeka
- version : v2.6.1

 
Ne pas oublier de faire un `composer install` pour mettre à jour les dépendances.

Lors de l'installation, deux fichiers de configuration sont a documenter :

- `db.ini` 
- `application/config/config.ini`

À vérifier :

- l'existence du fichier `.htaccess` 
- l'appartenance et les droits d'accès en lecture & écriture pour `www-data` au répertoire `files`


#### Configuration https

#### Sécurisation Apache2


Dans le fichier .htaccess ou le serveur virtuel, interdire le parcours des répertoires cachés .git

```
RewriteRule ^.*\.git.* - [R=404]

<DirectoryMatch "^\.git">
    Require all denied
</DirectoryMatch>

<FilesMatch "^\.git">
    Require all denied
</FilesMatch>
```

### Plugins

#### Clean URL

- https://github.com/Daniel-KM/Omeka-plugin-CleanUrl
- version : 2.16.1 
- activé

#### COinS

- https://github.com/omeka/plugin-Coins
- version : v2.0.3
- non activé


#### Commenting

- https://github.com/omeka/plugin-Commenting
- version : v2.2
- activé

#### Contribution

La version git présente un bug, l'affichage d'un second formulaire de type de contribution ne s'affiche pas correctment après sélection

- https://github.com/omeka/plugin-Contribution
- v3.2
- activé

Utilisation de la version packagée :
- http://omeka.org/classic/plugins/Contribution/

#### CSS Editor 

- https://github.com/omeka/plugin-CSSEditor
- version : v1.0.1
-  activé

#### Edit Links

- https://github.com/ENS-ITEM/EditLinks
- version : courante (26 sept 2017)
- activé

#### Exhibit Builder

- https://github.com/omeka/plugin-ExhibitBuilder
- version : v3.4.1
- non activé

#### Geolocation

- https://github.com/omeka/plugin-Geolocation
- version : v2.2.6
- non activé 

#### Guest User

- https://github.com/omeka/plugin-GuestUser
- version : v1.1.3
- activé

#### Record Relations

- https://github.com/omeka/plugin-RecordRelations
- version : v2.0.1
- activé 

#### Redact Elements

- https://github.com/omeka/plugin-RedactElements
- version : v1.0
- non activé

#### Simple Contact

- https://github.com/Daniel-KM/Omeka-plugin-SimpleContact
- version : 0.7.0
- activé 

version modifiée et francisée par Daniel-KM 

#### Simple Page

- https://github.com/omeka/plugin-SimplePages
- v3.1
- activé 

#### Simple Vocab

- https://github.com/omeka/plugin-SimpleVocab
- version : v2.1
- activé

#### User Profiles

- https://github.com/omeka/plugin-UserProfiles
- version : v1.2.1
- activé

### Thèmes

#### Spécifique à omeka.fr

Dérivé du thème Season
 
- https://gitlab.com/OmekaFR/omeka-fr-seasons
- répertoire omeka-seasons 

#### Berlin

- https://github.com/omeka/theme-berlin
- version : v2.6 
- désactivé 

#### Thanks, Roy

- https://github.com/omeka/theme-thanksroy
- version : v2.5
- désactivé 

#### Seasons

- https://github.com/omeka/theme-seasons
- version : v2.5.1
- désactivé 
